import lombok.Data;

/**
 * 求贤若饥 虚心若愚
 *
 * @author jokerliang
 * @date 2020-06-01 15:01
 */
@Data
public class GashaponShipmentReportEntity {
    private String deviceCode;

    /**
     * 订单号是什么,
     */
    private String orderId;

    /**
     * 出货仓位号
     */
    private int space;

    /**
     * 当前是否出货成功
     */
    private Boolean success;

    /**
     * 如果出货失败，原因是什么
     */
    private String errorMessage;

    /**
     * 附加消息
     */
    private String message;
}
