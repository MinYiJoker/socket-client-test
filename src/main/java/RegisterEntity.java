import lombok.Data;

/**
 * 求贤若饥 虚心若愚
 *
 * @author jokerliang
 * @date 2020-06-01 15:19
 */
@Data
public class RegisterEntity {
    private String deviceCode;
}
