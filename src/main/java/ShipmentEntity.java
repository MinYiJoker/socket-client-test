import lombok.Data;

/**
 * 求贤若饥 虚心若愚
 *
 * @author jokerliang
 * @date 2020-05-27 16:18
 */
@Data
public class ShipmentEntity {
    /**
     * 订单号
     */
    private String orderId;

    /**
     * 出货数量
     */
    private int number;

    /**
     * 仓位号
     */
    private int space;
}
