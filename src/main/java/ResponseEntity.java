import lombok.Data;

/**
 * 求贤若饥 虚心若愚
 *
 * @author jokerliang
 * @date 2020-06-01 15:29
 */
@Data
public class ResponseEntity {
    private Boolean rel;
    private String message;
}
